﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoVolador : MonoBehaviour
{

    public float speed;
    public float length;
   
    private float counter;
    private float lastPosition;
    private float startPosition;

    // Start is called before the first frame update
    void Start()
    {
        
        counter = speed * 0.03f;
        startPosition = transform.position.y;
        lastPosition = startPosition + length;
    }

    // Update is called once per frame
    void Update()
    {

       if (transform.position.y < lastPosition)
       {
            transform.position = new Vector2(transform.position.x , transform.position.y + counter);
            lastPosition = startPosition + length;
        }
       else if(transform.position.y == lastPosition)
       {
            lastPosition = startPosition;
       }
       else if (transform.position.y > lastPosition)
       {
            transform.position = new Vector2(transform.position.x, transform.position.y - counter);
            lastPosition = startPosition;
       }
       else if (transform.position.y == startPosition)
       {
            lastPosition = startPosition + length;
       }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Bala aliado"))
        {

            Object.Destroy(gameObject);
        }
    }
}
