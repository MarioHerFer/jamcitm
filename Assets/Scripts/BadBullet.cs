﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadBullet : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        direccion(transform.localScale.x);

        Invoke("dead", 3f);
    }

    void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.CompareTag("Player"))
        {
            //Object.Destroy(gameObject);
            Invoke("dead", 0.02f);
        }
    }

    void dead()
    {
        Object.Destroy(gameObject);
    }

    void direccion(float dir)
    {
        rb.velocity = (transform.right * dir) * speed;

        if (dir > 0)
        {
            dir = 1;
        }
        else
        {
            dir = -1;
        }
        Vector3 Scale = transform.localScale;
        Scale.x *= dir;
        transform.localScale = Scale;

        this.transform.parent = null;
    }

}