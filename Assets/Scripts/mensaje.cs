﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class mensaje : MonoBehaviour
{

    [SerializeField] GameObject textNPC;
    private TextMeshProUGUI texto;
    private Image imagen;
    [Header("Colores")]
    public Color visible;
    public Color invisible;

    public string elMensaje;
    // Start is called before the first frame update
    void Start()
    {
        texto = textNPC.GetComponent<TextMeshProUGUI>();
        imagen = textNPC.GetComponentInChildren<Image>();
    }
    // Update is called once per frame
    void Update(){
        
    }


    private void OnTriggerEnter(Collider other){

        if (other.gameObject.CompareTag("Player")){
            texto.SetText(elMensaje);
            imagen.color = invisible;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            texto.SetText("");
            imagen.color = visible;
        }
    }
}
