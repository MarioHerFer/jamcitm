﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    Rigidbody rb;
    private float x;
    private float xR;
    public float jumpForce;
    private bool jump;
    private bool Right = true;

    public float speed;

    public bool enElSuelo;
    // Start is called before the first frame update
    void Start(){
        rb = GetComponent<Rigidbody>();
        enElSuelo = false;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        x = Input.GetAxisRaw("Horizontal") * speed;
        jump = Input.GetKeyDown(KeyCode.W);

       
        rb.velocity = new Vector2(x, rb.velocity.y);
        
    }
    void Update(){

        if (jump && enElSuelo){
            rb.velocity = Vector2.up * jumpForce;
        }

        if (Right == false && x > 0){
            Flip();
        }
        else if (Right == true && x < 0){
            Flip();
        }

    }

    void Flip()
    {
        Right = !Right;
        Vector3 Scale = transform.localScale;
        Scale.x *= -1;
        transform.localScale = Scale;

    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Suelo")
        {
            enElSuelo = true;
        }

        if (other.gameObject.tag == "Suelo")
        {
            enElSuelo = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Suelo")
        {
            enElSuelo = false;
        }
    }
}
