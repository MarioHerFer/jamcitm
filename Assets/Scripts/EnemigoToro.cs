﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoToro : MonoBehaviour
{

    public float speed;
    public float dash;
    public float length;

    private float counter;
    private float counterDash;
    private float lastPosition;
    private float startPosition;
    private bool detect;

    // Start is called before the first frame update
    void Start()
    {
        detect = false;
        counterDash = dash * 0.03f;
        counter = speed * 0.03f;
        startPosition = transform.position.x;
        lastPosition = startPosition + length;
    }

    // Update is called once per frame
    void Update()
    {
        if (detect == false)
        {
            if (transform.position.x < lastPosition)
            {
                transform.position = new Vector2(transform.position.x + counter, transform.position.y);
                lastPosition = startPosition + length;
            }
            else if (transform.position.x == lastPosition)
            {
                lastPosition = startPosition;
            }
             if (transform.position.x > lastPosition)
            {
                transform.position = new Vector2(transform.position.x - counter, transform.position.y);
                lastPosition = startPosition;
            }
            else if (transform.position.x == startPosition)
            {
                lastPosition = startPosition + length;
            }
        }
        else
        {
            if (transform.position.x < lastPosition)
            {
                transform.position = new Vector2(transform.position.x + counterDash, transform.position.y);
                lastPosition = startPosition + length;
            }
            else if (transform.position.x == lastPosition)
            {
                lastPosition = startPosition;
            }
             if (transform.position.x > lastPosition)
            {
                transform.position = new Vector2(transform.position.x - counterDash, transform.position.y);
                lastPosition = startPosition;
            }
            else if (transform.position.x == startPosition)
            {
                lastPosition = startPosition + length;
            }
        }
    }


    void OnTriggerEnter(Collider other){

        if (other.gameObject.CompareTag("Player"))
        {
            detect = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            detect = false;
        }
    }

 
}
