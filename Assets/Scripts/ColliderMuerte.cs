﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderMuerte : MonoBehaviour
{
    [SerializeField] Count count;
    public float vidas = 1;

    private void Start()
    {
        
    }
        // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Bala aliado")){
            vidas--;
            if (vidas == 0){
                transform.parent.gameObject.SetActive(false);
                count.mas1asses();
            }
            Destroy(other);
        }
    }
}
