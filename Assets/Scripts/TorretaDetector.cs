﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorretaDetector : MonoBehaviour
{
    private bool Right = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.gameObject.transform.position.x > this.gameObject.transform.position.x && Right == false)
            {
                Vector3 Scale = transform.parent.localScale;
                Scale.x *= -1;
                transform.parent.localScale = Scale;
                Right = true;
            }
            else if (other.gameObject.transform.position.x < this.gameObject.transform.position.x && Right == true)
            {
                Vector3 Scale = transform.parent.localScale;
                Scale.x *= -1;
                transform.parent.localScale = Scale;
                Right = false;
            }

        }
    }
}
