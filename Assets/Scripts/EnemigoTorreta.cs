﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoTorreta : MonoBehaviour
{

    private bool detect;
    // Start is called before the first frame update
    void Start()
    {
        detect = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            detect = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            detect = false;
        }
    }
}
