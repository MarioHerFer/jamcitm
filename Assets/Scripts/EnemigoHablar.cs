﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoHablar : MonoBehaviour
{
    public float speed;
    private float counter;
  


    // Start is called before the first frame update
    void Start()
    {
        counter = speed * 0.03f;
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.position = new Vector2(transform.position.x - counter, transform.position.y);

    }

    
}
