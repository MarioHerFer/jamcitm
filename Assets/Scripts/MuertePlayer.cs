﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MuertePlayer : MonoBehaviour
{

    [SerializeField] Count count;

    // Update is called once per frame
    void Update()
    {
    }


    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Bala enemy")){

            resetear();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Finish") && count.cuantosAssess() == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            resetear();
        }

    }


    void resetear()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
