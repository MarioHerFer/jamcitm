﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoWeapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float cadencia;
    private float time;
    // Update is called once per frame


    void Update()
    {

        time -= Time.deltaTime;

        if (time <=0){
            Shoot();
            time = cadencia;
        }

    }

    void Shoot()
    {
        var TheBullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

        TheBullet.transform.parent = gameObject.transform;
    }


}
