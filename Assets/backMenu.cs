﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class backMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Back",5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Back()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);

    }
}
